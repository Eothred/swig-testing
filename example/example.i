/* example.i */
%module example
%{
extern double my_variable;
extern int fact(int n);
extern int my_mod(int x, int y);
extern char *get_time();

%}

double my_variable;
int fact(int n);
int my_mod(int x, int y);
char *get_time();