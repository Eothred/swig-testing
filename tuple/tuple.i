/* tuple.i */
%module tuple
%include "typemaps.i";
%apply double * OUTPUT {double *OUT1};
%apply int * OUTPUT {int *OUT2};
%inline %{
extern void my_func(int n, double * OUT1, int * OUT2);

%}


void my_func(int n, double * OUT1, int * OUT2);