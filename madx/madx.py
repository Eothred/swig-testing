
import madx_swig,atexit

class madx():
    def __init__(self):
        madx_swig.madx_start()
        atexit.register(madx_swig.madx_finish)

    def cmd(self,command):
        '''
        Send a command to Mad-X
        '''
        madx_swig.pro_input_(command.lower())
    def get_var(self,varname):
        '''
        Get the value of varname
        '''
        return madx_swig.get_variable_(varname)


if __name__=="__main__":
    m=madx()
    m.cmd("help,call;")

