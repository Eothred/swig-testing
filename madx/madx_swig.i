/* madx_swig.i */

%module madx_swig
%{
#include <mad_def.h>
#include <mad_core.h>
#include <mad_eval.h>
#include <mad_var.h>
%}

void set_variable_(char* name, double* value);
double get_variable_(char* name);
void pro_input_(char* statement);
void madx_start();
void madx_finish();

