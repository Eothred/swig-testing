/* File : mylib.cc */

#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

#include "mylib.h"

Vector* xprod(Vector* A, Vector* B) {
    Vector *ret;
    ret->x=0;
    for (int i=0;i<3;i++)
    {
        ret->x=A->y*B->z-A->z*B->y;
        ret->y=A->x*B->z-A->z*B->x;
        ret->z=A->x*B->y-A->y*B->x;
    }
    return ret;
}
// Vector mat_add(Vector A, Vector B) {
//     check_size(A,B);
//     Vector ret = create_matrix("mult",A.size);
//     for (int i=0;i<A.size;i++)
//     {
//         for(int j=0;j<A.size;i++)
//         {
//             ret.data[i][j]=A.data[i][j]+B.data[i][j];
//         }
//     }
//     return ret;
// }
// Vector mat_subtract(Vector A, Vector B) {
//     check_size(A,B);
//     Vector ret = create_matrix("mult",A.size);
//     for (int i=0;i<A.size;i++)
//     {
//         for(int j=0;j<A.size;i++)
//         {
//             ret.data[i][j]=A.data[i][j]-B.data[i][j];
//         }
//     }
//     return ret;
// }