# Try to find the build flags to compile octave shared objects (oct and mex files)
# Once done this will define
#
# OCTAVE_FOUND - if Coin3d is found
# OCTAVE_CXX_FLAGS - extra flags
# OCTAVE_INCLUDE_DIRS - include directories
# OCTAVE_LINK_DIRS - link directories
# OCTAVE_LIBRARY_RELEASE - the relase version
# OCTAVE_LIBRARY_DEBUG - the debug version
# OCTAVE_LIBRARY - a default library, with priority debug.
# OCTAVE_VERSION - The version of the library
# OCTAVE_PREFIX - Optional input, specify prefix path to octave installation
#                 e.g. -DOCTAVE_PREFIX=/opt if octave-config is in /opt/bin

IF (OCTAVE_FOUND) 
    SET(OCTAVE_FIND_QUIETLY TRUE)
ENDIF (OCTAVE_FOUND) 

IF (OCTAVE_PREFIX)
    SET(_octave_prefixbin ${OCTAVE_PREFIX}/bin)
ENDIF (OCTAVE_PREFIX)

# use mkoctfile
set(MKOCTFILE_EXECUTABLE MKOCTFILE_EXECUTABLE-NOTFOUND)
find_program(MKOCTFILE_EXECUTABLE NAME mkoctfile HINTS ${_octave_prefixbin} PATHS ${PATHS})
mark_as_advanced(MKOCTFILE_EXECUTABLE)

# use octave_config
set(OCTAVE_CONFIG_EXECUTABLE OCTAVE_CONFIG_EXECUTABLE-NOTFOUND)
find_program(OCTAVE_CONFIG_EXECUTABLE NAME octave-config HINTS ${_octave_prefixbin} PATHS ${PATHS})
mark_as_advanced(OCTAVE_CONFIG_EXECUTABLE)


if(MKOCTFILE_EXECUTABLE)
set(OCTAVE_FOUND 1)

execute_process(
COMMAND ${MKOCTFILE_EXECUTABLE} -p ALL_CXXFLAGS
OUTPUT_VARIABLE _mkoctfile_cppflags
RESULT_VARIABLE _mkoctfile_failed)
string(REGEX REPLACE "[\r\n]" " " _mkoctfile_cppflags "${_mkoctfile_cppflags}")
execute_process(
COMMAND ${MKOCTFILE_EXECUTABLE} -p INCFLAGS
OUTPUT_VARIABLE _mkoctfile_includedir
RESULT_VARIABLE _mkoctfile_failed)
string(REGEX REPLACE "[\r\n]" " " _mkoctfile_includedir "${_mkoctfile_includedir}")
string(REGEX REPLACE "-I" " " _mkoctfile_includedir "${_mkoctfile_includedir}")
execute_process(
COMMAND ${MKOCTFILE_EXECUTABLE} -p ALL_LDFLAGS
OUTPUT_VARIABLE _mkoctfile_ldflags
RESULT_VARIABLE _mkoctfile_failed)
string(REGEX REPLACE "[\r\n]" " " _mkoctfile_ldflags "${_mkoctfile_ldflags}")
execute_process(
COMMAND ${MKOCTFILE_EXECUTABLE} -p LFLAGS
OUTPUT_VARIABLE _mkoctfile_lflags
RESULT_VARIABLE _mkoctfile_failed)
string(REGEX REPLACE "[\r\n]" " " _mkoctfile_lflags "${_mkoctfile_lflags}")
execute_process(
COMMAND ${MKOCTFILE_EXECUTABLE} -p LIBS
OUTPUT_VARIABLE _mkoctfile_libs
RESULT_VARIABLE _mkoctfile_failed)
string(REGEX REPLACE "[\r\n]" " " _mkoctfile_libs "${_mkoctfile_libs}")
execute_process(
COMMAND ${MKOCTFILE_EXECUTABLE} -p OCTAVE_LIBS
OUTPUT_VARIABLE _mkoctfile_octlibs
RESULT_VARIABLE _mkoctfile_failed)
string(REGEX REPLACE "[\r\n]" " " _mkoctfile_octlibs "${_mkoctfile_octlibs}")
set(_mkoctfile_libs "${_mkoctfile_libs} ${_mkoctfile_octlibs}")

string(REGEX MATCHALL "(^| )-l([./+-_\\a-zA-Z]*)" _mkoctfile_libs "${_mkoctfile_libs}")
string(REGEX REPLACE "(^| )-l" "" _mkoctfile_libs "${_mkoctfile_libs}")

string(REGEX MATCHALL "(^| )-L([./+-_\\a-zA-Z]*)" _mkoctfile_ldirs "${_mkoctfile_lflags}")
string(REGEX REPLACE "(^| )-L" "" _mkoctfile_ldirs "${_mkoctfile_ldirs}")

string(REGEX REPLACE "(^| )-l([./+-_\\a-zA-Z]*)" " " _mkoctfile_ldflags "${_mkoctfile_ldflags}")
string(REGEX REPLACE "(^| )-L([./+-_\\a-zA-Z]*)" " " _mkoctfile_ldflags "${_mkoctfile_ldflags}")

separate_arguments(_mkoctfile_includedir)

set( OCTAVE_CXX_FLAGS " ${_mkoctfile_cppflags}" )
set( OCTAVE_LINK_FLAGS " ${_mkoctfile_ldflags} " )
# YIL fix in order not to include /path/to/octave-ver/octave..
foreach(_incldir ${_mkoctfile_includedir})
    if(NOT ${_incldir} MATCHES "octave$")
        set( OCTAVE_INCLUDE_DIRS ${OCTAVE_INCLUDE_DIRS} ${_incldir})
    endif()
endforeach()
set( OCTAVE_LINK_DIRS ${_mkoctfile_ldirs})
set( OCTAVE_LIBRARY ${_mkoctfile_libs})
set( OCTAVE_LIBRARY_RELEASE " ${OCTAVE_LIBRARY} ")
set( OCTAVE_LIBRARY_DEBUG " ${OCTAVE_LIBRARY} ")
endif(MKOCTFILE_EXECUTABLE)
if(OCTAVE_CONFIG_EXECUTABLE)
execute_process(
COMMAND ${OCTAVE_CONFIG_EXECUTABLE} -p CANONICAL_HOST_TYPE
OUTPUT_VARIABLE _octave_config_host_type
RESULT_VARIABLE _octave_config_failed)
string(REGEX REPLACE "[\r\n]""" _octave_config_host_type "${_octave_config_host_type}")
execute_process(
COMMAND ${OCTAVE_CONFIG_EXECUTABLE} -p API_VERSION
OUTPUT_VARIABLE _octave_config_api_version
RESULT_VARIABLE _octave_config_failed)
string(REGEX REPLACE "[\r\n]""" _octave_config_api_version "${_octave_config_api_version}")
execute_process(
COMMAND ${OCTAVE_CONFIG_EXECUTABLE} -v
OUTPUT_VARIABLE _octave_config_version
RESULT_VARIABLE _octave_config_failed)
string(REGEX REPLACE "[\r\n]""" _octave_config_version "${_octave_config_version}")
execute_process(
COMMAND ${OCTAVE_CONFIG_EXECUTABLE} -p LOCALVEROCTFILEDIR
OUTPUT_VARIABLE _octave_config_localveroctfiledir
RESULT_VARIABLE _octave_config_failed)
string(REGEX REPLACE "[\r\n]""" _octave_config_localveroctfiledir "${_octave_config_api_version}")

set( OCTAVE_HOST_TYPE "${_octave_config_host_type}" )
set( OCTAVE_API_VERSION "${_octave_config_api_version}" )
set( OCTAVE_VERSION "${_octave_config_version}" )
string(REGEX MATCH "[0-9]+" OCTAVE_VERSION_MAJOR "${OCTAVE_VERSION}")
string(REGEX REPLACE "[0-9]+\\.([0-9]+)\\.[0-9]+" "\\1" OCTAVE_VERSION_MINOR "${OCTAVE_VERSION}")
string(REGEX REPLACE "[0-9]+\\.[0-9]+\\.([0-9]+)" "\\1" OCTAVE_VERSION_PATCHLEVEL "${OCTAVE_VERSION}")
set( OCTAVE_LOCALVEROCTFILEDIR "${_octave_config_localveroctfiledir}" )

endif(OCTAVE_CONFIG_EXECUTABLE)

if(OCTAVE_FOUND AND NOT OCTAVE_FIND_QUIETLY)
    message(STATUS "Found Octave: ${OCTAVE_VERSION_MAJOR}.${OCTAVE_VERSION_MINOR}.${OCTAVE_VERSION_PATCHLEVEL}")
endif()

MARK_AS_ADVANCED(
OCTAVE_LIBRARY_FOUND
OCTAVE_CXX_FLAGS
OCTAVE_LINK_FLAGS
OCTAVE_INCLUDE_DIRS
OCTAVE_LINK_DIRS
OCTAVE_LIBRARY
OCTAVE_LIBRARY_RELEASE
OCTAVE_LIBRARY_DEBUG
)

