
macro(swig_octave_module name)
    find_package(Octave)
    include_directories(${OCTAVE_INCLUDE_DIRS})
    link_directories(${OCTAVE_LINK_DIRS})

    unset(swig_dot_i_sources)
    unset(swig_other_sources)

    foreach(_incldir ${OCT_SWIG_INCLUDE_DIRS})
        set(_inclarg ${_inclarg} "-I${_incldir}")
    endforeach()
    foreach(_lib ${OCT_SWIG_LIBS})
        set(_libarg ${_libarg} "-Wl,${_lib}")
    endforeach()
    foreach(_ldir ${OCT_SWIG_LIBDIRS})
        set(_libarg ${_libarg} "-Wl,-rpath,${_ldir}")
    endforeach()

    set(_wrap_target ${CMAKE_CURRENT_BINARY_DIR}/CMakeFiles/${name}.oct.dir/${name}_OCTAVE_wrap.cpp)
    foreach(it ${ARGN})
        if(${it} MATCHES ".\\.i$")
            set(swig_dot_i_sources ${swig_dot_i_sources} "${CMAKE_CURRENT_SOURCE_DIR}/${it}")
        else()
            set(swig_other_sources ${swig_other_sources} "${CMAKE_CURRENT_SOURCE_DIR}/${it}")
        endif()
    endforeach()
    add_custom_command(OUTPUT ${_wrap_target}
        COMMAND swig -octave -c++ -o ${_wrap_target} ${swig_dot_i_sources} ${swig_other_sources}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        DEPENDS ${ARGN}
    )
    set(_target_name ${name}_oct)
    add_custom_target(${name}.oct ALL
        ${MKOCTFILE_EXECUTABLE} ${_wrap_target} ${swig_other_sources} ${_inclarg} ${_libarg} -o ${name}.oct
        DEPENDS ${_wrap_target}
    )
endmacro()