cmake_minimum_required(VERSION 2.8)

project(swig_tests)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

add_subdirectory(example)
add_subdirectory(madx)
add_subdirectory(tuple)

add_subdirectory(mylib)
add_subdirectory(myinterface)

