/* myinterface.i */
%module myinterface
%include "typemaps.i";
%{
#include <mylib.h>
%}

typedef struct {
        double x,y,z;
        %extend {
            Vector(double x, double y, double z) {
                Vector *v;
                v=(Vector *) malloc(sizeof(Vector));
                v->x=x;
                v->y=y;
                v->z=z;
                return v;
            }
            ~Vector() { 
                free($self);
            }
            void prt() {
                printf("Yngve::Vector [%g, %g, %g]\n", $self->x,$self->y,$self->z);
            }
        }
} Vector;

Vector * xprod(Vector* A,Vector* B);